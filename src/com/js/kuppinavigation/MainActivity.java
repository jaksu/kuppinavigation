package com.js.kuppinavigation;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.js.kuppinavigation.dialog.AboutDialogFragment;
import com.js.kuppinavigation.service.LocationService;
import com.js.kuppinavigation.utils.SettingsUtil;

public class MainActivity extends Activity {
	private static final String TAG = "MainActivity";

	private Button startTackingButton;
	private boolean isBound = false;

	private Messenger serviceMessenger = null;
	private final Messenger activityMessenger = new Messenger(new IncomingHandler(this));
	private ServiceConnection serviceConnection = new LocationServiceConnection();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		init();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		Log.v(TAG, "Destroy kuppi navigation app");
		Log.v(TAG, "Service status : " + LocationService.isStarted());

		if (LocationService.isStarted())
			stopLocationService();

		Editable urlEdit = ((EditText) findViewById(R.id.editUrl)).getText();
		if (urlEdit != null) {
			Log.v(TAG, "Save default url to: " + urlEdit.toString());
			SettingsUtil.getInstance(this).saveStringToSettings(SettingsUtil.SETTING_KEY_DEFAULT_URL, urlEdit.toString());
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.v(TAG, "onStop, Service status : " + LocationService.isStarted());
	}

	private void init() {
		startTackingButton = (Button) findViewById(R.id.startTrackingButton);
		startTackingButton.setOnClickListener(new StartTrackingButtonListener());

		loadSettings();
	}

	private void loadSettings() {
		String defaultUrl = SettingsUtil.getInstance(this).getStringFromSettings(SettingsUtil.SETTING_KEY_DEFAULT_URL);

		TextView editUrl = (TextView) findViewById(R.id.editUrl);
		editUrl.setText(defaultUrl);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.labelGpsInterval) {
			showSettingsActivity();
			return true;
		}
		if (id == R.id.action_about) {
			showAboutDialog();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void showSettingsActivity() {
		Intent settings = new Intent(MainActivity.this, SettingsActivity.class);
		startActivity(settings);
	}

	private void showAboutDialog() {
		DialogFragment newFragment = AboutDialogFragment.newInstance();
		newFragment.show(getFragmentManager(), "dialog");
	}

	private void startLocationservice() {
		Log.v(TAG, "Start location service");

		hideKeyboard();

		Intent intent = new Intent(MainActivity.this, LocationService.class);
		intent.putExtra("url", getTextFieldValue(R.id.editUrl));
		intent.putExtra("name", getTextFieldValue(R.id.editName));
		intent.putExtra("runnerNumber", getTextFieldValue(R.id.editRunner));
		intent.putExtra("password", getTextFieldValue(R.id.editPassword));

		startService(intent);
		doBindService();
	}

	private void hideKeyboard() {
		Log.v(TAG, "Hide keyboard");
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(startTackingButton.getWindowToken(), 0);
	}

	private String getTextFieldValue(int editname) {
		if (((EditText) findViewById(editname)).getText() != null)
			return ((EditText) findViewById(editname)).getText().toString();
		else
			return "";
	}

	private void stopLocationService() {
		Log.v(TAG, "Stop location service");
		doUnbindService();
		stopService(new Intent(MainActivity.this, LocationService.class));
	}

	void doBindService() {
		Log.v(TAG, "Ui bind to service");
		bindService(new Intent(this, LocationService.class), serviceConnection, Context.BIND_AUTO_CREATE);
		isBound = true;
	}

	void doUnbindService() {
		Log.v(TAG, "Ui unbind from service");
		if (isBound) {
			// If we have received the service, and hence registered with it,
			// then now is the time to unregister.
			if (serviceMessenger != null) {
				try {
					Message msg = Message.obtain(null, LocationService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = activityMessenger;
					serviceMessenger.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has
					// crashed.
				}
			}
			// Detach our existing connection.
			unbindService(serviceConnection);
			isBound = false;
		}
	}

	private class StartTrackingButtonListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			setValueAndSwitch(R.id.viewUrlSwitcher, R.id.editUrl, R.id.labelUrl);
			setValueAndSwitch(R.id.viewNameSwitcher, R.id.editName, R.id.labelName);
			setValueAndSwitch(R.id.viewRunnerSwitcher, R.id.editRunner, R.id.labelRunner);
			setValueAndSwitch(R.id.viewPasswordSwitcher, R.id.editPassword, R.id.labelPassword, "****");

			setFiledVisible(R.id.textServerResponse);
			setFiledVisible(R.id.textCounters);

			if (LocationService.isStarted()) {
				stopLocationService();
				startTackingButton.setText(R.string.start_tracking);
			} else {
				startLocationservice();
				startTackingButton.setText(R.string.stop_tracking);
			}
		}

		private void setFiledVisible(int fieldId) {
			TextView textLabel = (TextView) findViewById(fieldId);
			textLabel.setVisibility(View.VISIBLE);
		}

		private void setValueAndSwitch(int viewSwitcherId, int editId, int labelId, String labelText) {
			TextView textLabel = (TextView) findViewById(labelId);
			EditText editField = (EditText) findViewById(editId);

			if (labelText == null || labelText.length() == 0)
				textLabel.setText(editField.getText());
			else
				textLabel.setText(labelText);

			ViewSwitcher switcher = (ViewSwitcher) findViewById(viewSwitcherId);
			switcher.showNext();

		}

		private void setValueAndSwitch(int viewSwitcherId, int editId, int labelId) {
			setValueAndSwitch(viewSwitcherId, editId, labelId, null);
		}
	}

	static class IncomingHandler extends Handler {

		private MainActivity mainActivity;

		public IncomingHandler(MainActivity mainActivity) {
			this.mainActivity = mainActivity;
		}

		@Override
		public void handleMessage(Message msg) {
			Log.v(TAG, "Ui handle incoming message");
			Log.v(TAG, "message what: " + msg.what);
			switch (msg.what) {
			case LocationService.MSG_SET_STRING_VALUE:
				if (isMessage(LocationService.URL_RESPONCE_KEY, msg))
					handleServerResponce(msg.getData().getString(LocationService.URL_RESPONCE_KEY));
				if (isMessage(LocationService.COUNTER_KEY, msg))
					handleCounterResponce(msg.getData().getString(LocationService.COUNTER_KEY));
				if (isMessage(LocationService.LOCATION_KEY, msg))
					handleLocationResponce(msg.getData().getString(LocationService.LOCATION_KEY));

				break;
			default:
				super.handleMessage(msg);
			}
		}

		private void handleLocationResponce(String locationStr) {
			TextView textView = (TextView) mainActivity.findViewById(R.id.labelLocation);
			textView.setText(locationStr);
		}

		private void handleCounterResponce(String counterStr) {
			TextView textView = (TextView) mainActivity.findViewById(R.id.textCounter);
			textView.setText(counterStr);
		}

		private void handleServerResponce(String responce) {
			Log.v(TAG, "Ui handle server responce");
			TextView textView = (TextView) mainActivity.findViewById(R.id.textHttpResult);
			textView.setText(responce);
		}

		private boolean isMessage(String responceKey, Message msg) {
			String responce = msg.getData().getString(responceKey);
			Log.v(TAG, "Is message, responce key: " + responceKey + ", responce message: " + responce);
			if (responce != null)
				return true;

			return false;
		}
	}

	class LocationServiceConnection implements ServiceConnection {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			serviceMessenger = new Messenger(service);
			try {
				Message msg = Message.obtain(null, LocationService.MSG_REGISTER_CLIENT);
				msg.replyTo = activityMessenger;
				serviceMessenger.send(msg);

				Log.v(TAG, "Ui connected to service");
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do
				// anything with it
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected - process crashed.
			serviceMessenger = null;

			Log.v(TAG, "Ui disconnected from service");
		}
	}
}
