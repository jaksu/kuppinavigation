package com.js.kuppinavigation;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.js.kuppinavigation.utils.SettingsUtil;

public class SettingsActivity extends Activity {
	private static final String TAG = "MainActivity";

	private Button saveButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_layout);

		loadSettings();

		saveButton = (Button) findViewById(R.id.settingsSaveButton);
		saveButton.setOnClickListener(new SaveButtonListener());
	}

	private void loadSettings() {
		int defaultInterval = SettingsUtil.getInstance(this).getIntFromSettings(SettingsUtil.SETTING_KEY_INTERVAL);
		float defaultDistance = SettingsUtil.getInstance(this).getFloatFromSettings(SettingsUtil.SETTING_KEY_DISTANCE);

		EditText editInterval = (EditText) findViewById(R.id.editInterval);
		editInterval.setText(Integer.toString(defaultInterval));

		EditText editDistance = (EditText) findViewById(R.id.editDistance);
		editDistance.setText(Float.toString(defaultDistance));
	}

	class SaveButtonListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			Editable editInterval = ((EditText) findViewById(R.id.editInterval)).getText();
			if (editInterval != null) {
				String interval = editInterval.toString();
				if (interval.toString().length() == 0)
					interval = "0";

				Log.v(TAG, "Save default interval to: " + editInterval.toString());
				SettingsUtil.getInstance(SettingsActivity.this).saveIntToSettings(SettingsUtil.SETTING_KEY_INTERVAL, Integer.parseInt(interval));
			}

			Editable editDistance = ((EditText) findViewById(R.id.editDistance)).getText();
			if (editDistance != null) {
				String distance = editDistance.toString();
				if (distance.toString().length() == 0)
					distance = "0";

				Log.v(TAG, "Save default distance to: " + editDistance.toString());
				SettingsUtil.getInstance(SettingsActivity.this).saveFloatToSettings(SettingsUtil.SETTING_KEY_DISTANCE, Float.parseFloat(distance));
			}

			SettingsActivity.this.finish();
		}
	}
}
