package com.js.kuppinavigation.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.js.kuppinavigation.R;

public class AboutDialogFragment extends DialogFragment {
	public static AboutDialogFragment newInstance() {
		return new AboutDialogFragment();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String message = getString(R.string.version) + ": " + getString(R.string.version_value);
		return new AlertDialog.Builder(getActivity()).setTitle(R.string.about).setMessage(message).setPositiveButton(R.string.dialog_ok, new OkClickListener()).create();
	}

	class OkClickListener implements DialogInterface.OnClickListener {

		@Override
		public void onClick(DialogInterface dialog, int which) {

		}
	}
}
