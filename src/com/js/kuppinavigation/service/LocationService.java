package com.js.kuppinavigation.service;

import java.util.ArrayList;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.js.kuppinavigation.service.http.HttpClient;
import com.js.kuppinavigation.utils.SettingsUtil;

public class LocationService extends Service {
	private static final String TAG = "LocationService";

	public static boolean isStarted = false;

	private static List<Messenger> clients = new ArrayList<Messenger>(); // Keeps track of all current registered clients.

	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;
	public static final int MSG_SET_STRING_VALUE = 3;

	public static final String URL_RESPONCE_KEY = "urlResponce";
	public static final String COUNTER_KEY = "counter";
	public static final String LOCATION_KEY = "location";

	private LocationManager locationManager = null;
	private Location lastLocation;

	private HttpClient httpClient;
	final Messenger messenger = new Messenger(new IncomingHandler()); // Target we publish for clients to send messages to IncomingHandler.

	@Override
	public void onCreate() {
		Log.v(TAG, "Location service is created");

		isStarted = true;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.v(TAG, "Location service is started");
		initService(intent);

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return messenger.getBinder();
	}

	private void initService(Intent intent) {
		createHttpClient(intent);

		initializeLocationManager();

	}

	private void sendMessageToUI(int messageType, String messageKey, String message) {
		for (int i = clients.size() - 1; i >= 0; i--) {
			try {
				// Send data as an Integer
				// clients.get(i).send(Message.obtain(null, messageType, intvaluetosend, 0));

				// Send data as a String
				Log.v(TAG, "Send message to ui. Message type: " + messageType + ", message key: " + messageKey + ", message: " + message);
				Bundle bundle = new Bundle();
				bundle.putString(messageKey, message);
				Message msg = Message.obtain(null, messageType);
				msg.setData(bundle);
				clients.get(i).send(msg);

			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				clients.remove(i);
			}
		}
	}

	private void createHttpClient(Intent intent) {
		Bundle extras = intent.getExtras();

		String url = extras.getString("url");
		String name = extras.getString("name");
		String runnerNumber = extras.getString("runnerNumber");
		String password = extras.getString("password");

		Log.v(TAG, "Create http client with params: Url: " + url + " Name: " + name + " Runner #: " + runnerNumber + " Password: " + password);

		// httpClient = new HttpClient("http://minibox.dy.fi/kuppi/kuppi_navigation.php", "Jouni", "passu", "2", new ServerResponse());
		httpClient = new HttpClient(url, name, password, runnerNumber, new ServerResponse());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.v(TAG, "Location service is stopped");

		httpClient = null;
		isStarted = false;
	}

	private void initializeLocationManager() {
		Log.v(TAG, "initializeLocationManager");
		if (locationManager == null) {
			locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
		}

		int interval = SettingsUtil.getInstance(this).getIntFromSettings(SettingsUtil.SETTING_KEY_INTERVAL, 2) * 1000;
		float distance = SettingsUtil.getInstance(this).getFloatFromSettings(SettingsUtil.SETTING_KEY_DISTANCE, 0);
		Log.v(TAG, "Inteval from settings: " + interval);
		Log.v(TAG, "Distance from settings: " + distance);

		try {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, interval, distance, new LocationListener(LocationManager.GPS_PROVIDER));
		} catch (java.lang.SecurityException ex) {
			Log.e(TAG, "fail to request location update, ignore", ex);
		} catch (IllegalArgumentException ex) {
			Log.e(TAG, "network provider does not exist, " + ex.getMessage());
		}
	}

	private class LocationListener implements android.location.LocationListener {
		public LocationListener(String provider) {
			Log.v(TAG, "LocationListener " + provider);
			lastLocation = new Location(provider);
		}

		@Override
		public void onLocationChanged(Location location) {
			Log.v(TAG, "onLocationChanged: " + location);
			lastLocation.set(location);

			if (httpClient != null) {
				httpClient.sendLocationDataToServer(location);
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
			Log.v(TAG, "onProviderDisabled: " + provider);
		}

		@Override
		public void onProviderEnabled(String provider) {
			Log.v(TAG, "onProviderEnabled: " + provider);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.v(TAG, "onStatusChanged: " + provider);
		}
	}

	private class ServerResponse implements HttpClient.ServerResponceCallback {

		@Override
		public void onResponce(String response) {
			// Todo implement error handling
			Log.v(TAG, "Location service handle response: " + response);
			sendMessageToUI(MSG_SET_STRING_VALUE, URL_RESPONCE_KEY, response);

			String location = buildLocationString();
			sendMessageToUI(MSG_SET_STRING_VALUE, LOCATION_KEY, location);

			String counter = buildCounterString();
			sendMessageToUI(MSG_SET_STRING_VALUE, COUNTER_KEY, counter);
		}

		private String buildLocationString() {
			StringBuilder builder = new StringBuilder();

			builder.append("Latitude: " + lastLocation.getLatitude() + "\n");
			builder.append("Longitude: " + lastLocation.getLongitude() + "\n");

			return builder.toString();
		}

		private String buildCounterString() {
			StringBuilder builder = new StringBuilder();

			builder.append("Send count: " + httpClient.getSendCounter() + "\n");
			builder.append("Error count: " + httpClient.getErrorCounter() + "\n");
			builder.append("Send total: " + httpClient.getSendTotalCounter() + "\n");
			builder.append("Error total: " + httpClient.getErrorTotalCounter() + "\n");

			return builder.toString();
		}
	}

	// Handler of incoming messages from clients.
	static class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				clients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				clients.remove(msg.replyTo);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	public static boolean isStarted() {
		return isStarted;
	}
}
