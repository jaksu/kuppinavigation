package com.js.kuppinavigation.service.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

public class HttpClient {

	private static final String TAG = "HttpClient";

	private String url;
	private String name;
	private String password;
	private String runnerNumber;

	private int sendCounter = 0;
	private static int sendTotalCounter = 0;
	private int errorCounter = 0;
	private static int errorTotalCounter = 0;

	private ServerResponceCallback serverResponceCallback;

	public HttpClient(String url, String name, String password, String runnerNumber, ServerResponceCallback serverResponceCallback) {
		this.url = url;
		this.name = name;
		this.password = password;
		this.runnerNumber = runnerNumber;
		this.serverResponceCallback = serverResponceCallback;

		sendCounter = 0;
		errorCounter = 0;
	}

	public void sendLocationDataToServer(Location location) {
		String url = buildUrl(location);

		new SendRequestTask().execute(url);
	}

	private String buildUrl(Location location) {
		Log.v(TAG, "Build url: " + url);
		Log.v(TAG, "With params. Name: " + name + " Runner#: " + runnerNumber + " Password: " + password);

		Long timestamp = location.getTime() / 1000;
		double latitude = Math.floor(location.getLatitude() * 1000000);
		double longitude = Math.floor(location.getLongitude() * 1000000);

		if (!url.startsWith("http://"))
			url = "http://" + url;

		if (url.endsWith("?"))
			url = url.substring(0, url.length() - 1);

		return url + "?act=s&n=" + name + "&c=" + runnerNumber + "&p=" + password + "&d=" + timestamp + "," + (int) latitude + "," + (int) longitude;
	}

	class SendRequestTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {
			URL url;
			StringBuffer response = new StringBuffer();

			try {
				url = new URL(urls[0]);

				Log.v(TAG, "Http client call URL: " + url);
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

				if (urlConnection.getResponseCode() == 200) {
					BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String inputLine;

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
				} else {
					BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
					String inputLine;

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					Log.v(TAG, "Error when send data. Error: " + response.toString());
					response.delete(0, response.length());
					response.append("Error!!");

					errorCounter++;
					errorTotalCounter++;
				}

				// log result
				Log.v(TAG, "Server response doInBackground: " + response.toString());

			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				errorCounter++;
				errorTotalCounter++;
				response.append("Unexpected http error!!");
			}

			return response.toString();
		}

		@Override
		protected void onPostExecute(String response) {
			Log.v(TAG, "Server response onPostExecute: " + response);

			sendCounter++;
			sendTotalCounter++;
			serverResponceCallback.onResponce(response);
		}
	}

	public int getSendCounter() {
		return sendCounter;
	}

	public int getSendTotalCounter() {
		return sendTotalCounter;
	}

	public int getErrorCounter() {
		return errorCounter;
	}

	public int getErrorTotalCounter() {
		return errorTotalCounter;
	}

	public interface ServerResponceCallback {
		public void onResponce(String response);
	}
}
