package com.js.kuppinavigation.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingsUtil {

	public static final String PREFERENCES_NAME = "KuppiPreferences";

	public static final String SETTING_KEY_DEFAULT_URL = "defaultUrl";
	public static final String SETTING_KEY_INTERVAL = "interval";
	public static final String SETTING_KEY_DISTANCE = "distance";

	private static SettingsUtil instance = null;
	private static Context context;

	private SettingsUtil(Context context) {
		SettingsUtil.context = context;
	}

	public static SettingsUtil getInstance(Context context) {
		if (instance == null) {
			instance = new SettingsUtil(context);
		}

		return instance;
	}

	public SharedPreferences getPreferences() {
		return context.getSharedPreferences(PREFERENCES_NAME, 0);
	}

	public void saveStringToSettings(String settingKey, String setting) {
		if (settingKey != null && setting != null) {
			// We need an Editor object to make preference changes.
			// All objects are from android.context.Context
			SharedPreferences settings = getPreferences();
			SharedPreferences.Editor editor = settings.edit();

			editor.putString(settingKey, setting);

			// Commit the edits!
			editor.commit();
		}
	}

	public void saveIntToSettings(String settingKey, int setting) {
		if (settingKey != null) {
			// We need an Editor object to make preference changes.
			// All objects are from android.context.Context
			SharedPreferences settings = getPreferences();
			SharedPreferences.Editor editor = settings.edit();

			editor.putInt(settingKey, setting);

			// Commit the edits!
			editor.commit();
		}
	}

	public void saveFloatToSettings(String settingKey, float setting) {
		if (settingKey != null) {
			// We need an Editor object to make preference changes.
			// All objects are from android.context.Context
			SharedPreferences settings = getPreferences();
			SharedPreferences.Editor editor = settings.edit();

			editor.putFloat(settingKey, setting);

			// Commit the edits!
			editor.commit();
		}
	}

	public String getStringFromSettings(String settingKey) {
		return getStringFromSettings(settingKey, "");
	}

	public String getStringFromSettings(String settingKey, String defaultValue) {
		SharedPreferences settings = getPreferences();
		String settingValue = settings.getString(settingKey, defaultValue);

		return settingValue;
	}

	public int getIntFromSettings(String settingKey) {
		return getIntFromSettings(settingKey, 0);
	}

	public int getIntFromSettings(String settingKey, int defaultValue) {
		SharedPreferences settings = getPreferences();
		int settingValue = settings.getInt(settingKey, defaultValue);

		return settingValue;
	}

	public float getFloatFromSettings(String settingKey) {
		return getFloatFromSettings(settingKey, 0);
	}

	public float getFloatFromSettings(String settingKey, float defaultValue) {
		SharedPreferences settings = getPreferences();
		float settingValue = settings.getFloat(settingKey, defaultValue);

		return settingValue;
	}
}
